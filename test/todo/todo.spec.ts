import { expect } from 'chai';
import { isRight } from 'fp-ts/lib/These';
import 'mocha';
import * as userRepo from '../../src/user/repo';
import * as F from '../env/factories';
import { getTestEnv } from '../env/testEnvironment';
import * as todoRepo from '../../src/todo/repo';


describe('Todo APIS',async () => {
    let testEnv;
    let user;
    let userId:Number;
    let authTokenResp:String;
    let todo,todoId;

    beforeEach(async () => {
        testEnv = await getTestEnv();
        todo = F.fakeTodo({});
    });


    //Login APIs
    it('User should be able to login',async () => {
        user = F.fakeUser({});
        const userIdResponse = await userRepo.saveUser(user);
        if (isRight(userIdResponse)) {
            userId = userIdResponse.right;
        }

    const response = await testEnv.server.inject({
        method: 'post',
        url: '/auth/login',
        payload: {
            email: user.email,
            password: user.password,
        }
    });
    authTokenResp = response.result.authToken;
    expect(response.statusCode).to.equal(200);
    expect(response.result).to.have.property('authToken');
    expect(response.result).to.have.property('userId').equal(userId);

    });

    //Create APIs
    it('It should create a todo item',async()=>{
        const response = await testEnv.server.inject({
            method:'post',
            url:'/todo',
            payload: todo,
            headers: { Authorization: authTokenResp },
        });
        todoId = response.result.todoId;
        expect(response.statusCode).to.equal(201);
        expect(response.result).to.have.property('todoId');
    });

    it('It should throw an error',async()=>{
        const response = await testEnv.server.inject({
            method:'post',
            url:'/todo',
            payload: {
                todoName: '',
                todoDescription: todo.todoDescription
            },
            headers: { Authorization: authTokenResp },
        });
        expect(response.statusCode).to.equal(400);
        expect(response.result).to.have.property('message').equal('Invalid request payload input');
    });

    //Delete APIs
    it('It should delete a todo item', async()=>{
        const response = await testEnv.server.inject({
            method:'delete',
            url:`/todo/${todoId}`,
            headers: { Authorization: authTokenResp }
        });
        expect(response.statusCode).to.equal(200);
        expect(response.result).to.equal('Todo Deleted !!');
    })
    it('It should not delete a todo item', async()=>{
        const response = await testEnv.server.inject({
            method:'delete',
            url:`/todo/${todoId}`,
            headers: { Authorization: authTokenResp }
        });
        expect(response.statusCode).to.equal(404);
        expect(response.result).to.equal('todo-id-not-available');
    });

    //Get All Todo Items APIs
    it('It should create a todo item',async()=>{
        const response = await testEnv.server.inject({
            method:'post',
            url:'/todo',
            payload: todo,
            headers: { Authorization: authTokenResp },
        });
        expect(response.statusCode).to.equal(201);
        expect(response.result).to.have.property('todoId');
    });
    it('It should return all the todo items',async()=>{
        const response = await testEnv.server.inject({
            method:'GET',
            url:'/todos',
            headers: { Authorization: authTokenResp }
        });
        expect(response.statusCode).to.equal(200);
        expect(response.result).to.be.a('array').with.length.greaterThan(0);
    })
})