import { logger } from './logger';
import _ from 'ramda';
import { level } from 'winston';

export type GeneralEvents = 'file-uploaded';

export type UserEvents =
    | 'user-account-created'
    | 'user-account-creation-failed'
    | 'user-login-successful'
    | 'user-login-failed';

export type CalculatorEvents = 'footprint-calculated';

export type StripeEvents = 'stripe-account-created';

export type LandscapeEvents = 'landscape-created' | 'landscape-updated';

export type MailchimpEvents =
    | 'subscribed-to-newsletter'
    | 'newsletter-subscription-failed'
    | 'mailchimp-language-updated';

export type TodoEvents = | 'todo-created' | 'todo-updated' | 'todo-listed' | 'todo-deleted' | 'todo-failed';

export type EventName =
    | GeneralEvents
    | UserEvents
    | CalculatorEvents
    | LandscapeEvents
    | MailchimpEvents
    | TodoEvents;


export function logEvent(
    eventName: EventName,
    eventData: any,
    level: string = 'info',
) {
    logger.log(level, { eventName, eventData });
}

export function fileUploadedEvent(fileDetails) {
    logEvent('file-uploaded', { fileDetails });
}

// User events

export function userAccountCreated(userDetails) {
    logEvent('user-account-created', userDetails);
}

export function userAccountCreationFailed(email) {
    logEvent('user-account-creation-failed', email);
}

export function userLoggedInEvent(userId) {
    logEvent('user-login-successful', { userId: userId });
}

export function userLoggedInFailed(reason) {
    logEvent('user-login-failed', { reason });
}

// Landscape function

export function landscapeCreatedEvent(id, landscapeName, metadata) {
    logEvent('landscape-created', { id, landscapeName, metadata });
}

// Calculator

export function footPrintCalculated(userAnswer, result) {
    logEvent('footprint-calculated', { userAnswer, result });
}

// Mail chimp

export function subscribedToNewsLetter(details) {
    logEvent('subscribed-to-newsletter', details);
}

export function mailchimpSubscriptionFailed(subscriberInfo, error) {
    logEvent(
        'newsletter-subscription-failed',
        { subscriberInfo, error },
        'error',
    );
}

export function mailchimpLanguageUpdated(email, language) {
    logEvent('mailchimp-language-updated', { email, language });
}

export function todoCreated(details){
    logEvent('todo-created',details);
}

export function todoDeleted(details){
    logEvent('todo-deleted',{details});
}

export function todoUpdated(details){
    logEvent('todo-updated',{details});
}

export function todoListed(details:object){
    logEvent('todo-listed',{details});
}

export function todoFailed(details){
    logEvent('todo-failed',{details});
}