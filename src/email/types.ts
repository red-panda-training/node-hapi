export interface EmailAttachment {
    fileName: String;
    base64Content: String;
}

export interface Email {
    toEmail: string[];
    ccEmail?: string[];
    subject: string;
    bodyHtml: string;
    emailType?: string;
    attachments?: EmailAttachment[];
}

// Emails

export interface EmailRecipient {
    firstName: string;
    email: string;
    userType: string;
    emailVerified: boolean;
}

export interface VerifyEmailAddressEmailData {
    recipient: EmailRecipient;
    verificationLink: string;
}

export interface EmailGateWay {
    sendEmail(email: Email): Promise<any>;
}
