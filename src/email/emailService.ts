import nunjucks from '../nunjucks';
import { Email, EmailGateWay, VerifyEmailAddressEmailData } from './types';

export default function emailService(emailGateWay: EmailGateWay) {
    return {
        sendVerifyEmailAddressEmail: async (
            verifyEmailAddressEmailData: VerifyEmailAddressEmailData,
        ) => {
            const emailBody = nunjucks.render('email/verifyEmailAddress.html', {
                ...verifyEmailAddressEmailData,
            });
            const email: Email = {
                toEmail: [verifyEmailAddressEmailData.recipient.email],
                subject: 'Verify email address',
                bodyHtml: emailBody,
                emailType: 'verifyEmailAddress',
            };
            const result = await emailGateWay.sendEmail(email);
        },
    };
}
