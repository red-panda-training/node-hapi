import { Email } from './types';
import SESEmailGateway from './SESGateway';
import config from '../config';

async function sendEmail() {
    const email: Email = {
        toEmail: [], // Add email address
        subject: 'Hello',
        bodyHtml: '<b> Hello </b> from Team',
        // attachments: [
        //     {
        //         fileName: "greeting.txt",
        //         base64Content: Buffer.from("Hola").toString("base64")
        //     }
        // ]
    };

    const gateway = SESEmailGateway(config);

    const response = await gateway.sendEmail(email);
    console.log(response);
}

sendEmail();
