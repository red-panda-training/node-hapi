import { Email, EmailGateWay } from './types';
import _ from 'ramda';
import SESV2, { SendEmailRequest } from 'aws-sdk/clients/sesv2';
import MailComposer from 'nodemailer/lib/mail-composer';
import { Either, left, right, isLeft } from 'fp-ts/lib/Either';
import { logger } from '../logger';

const generateRawMailData = (fromEmail, message: Email) => {
    let mailOptions = {
        from: fromEmail,
        to: message.toEmail,
        subject: message.subject,
        html: message.bodyHtml,
        attachments: message.attachments?.map((a) => ({
            filename: a.fileName,
            content: a.base64Content,
            encoding: 'base64',
        })),
    };
    return new MailComposer(mailOptions).compile().build();
};

const sendEmail = (awsSES: SESV2, config) => async (email: Email) => {
    const params: SendEmailRequest = {
        Content: {
            Raw: {
                Data: await generateRawMailData(
                    config.FROM_EMAIL_ADDRESS,
                    email,
                ),
            },
        },
        Destination: {
            ToAddresses: email.toEmail,
            CcAddresses: email.ccEmail,
        },
        FromEmailAddress: config.FROM_EMAIL_ADDRESS,
        ReplyToAddresses: [config.REPLY_TO_EMAIL_ADDRESS],
    };

    try {
        const response = await awsSES.sendEmail(params).promise();
        console.log(response);
        logger.info({
            eventName: 'email-sent',
            eventData: {
                emailType: email.emailType,
                recipientEmail: email.toEmail,
                messageId: response,
            },
        });
        return right(response);
    } catch (error) {
        logger.error({
            eventName: 'email-sending-failed',
            eventData: {
                emailType: email.emailType,
                recipientEmail: email.toEmail,
            },
        });
        console.log('Error in sending email', error);
    }
    return left('ERROR_IN_SENDING_EMAIL');
};

export default function SESEmailGateway(config): EmailGateWay {
    if (
        _.isNil(config.AWS_REGION) ||
        _.isNil(config.AWS_SECRET_ACCESS_KEY) ||
        _.isNil(config.AWS_ACCESS_KEY_ID)
    ) {
        throw new Error(
            'INVALID_AWS_CONFIGURATION while configuring SES Gateway',
        );
    }

    const SESConfig = {
        // apiVersion: '2019-09-27',
        accessKeyId: config.AWS_ACCESS_KEY_ID,
        secretAccessKey: config.AWS_SECRET_ACCESS_KEY,
        region: config.AWS_REGION,
    };

    let ses = new SESV2(SESConfig);

    return {
        sendEmail: sendEmail(ses, config),
    };
}
