/* eslint-disable import/prefer-default-export */
import * as Hapi from '@hapi/hapi';
import * as HapiSwagger from 'hapi-swagger';
import * as Inert from '@hapi/inert';
import * as Vision from '@hapi/vision';
import * as Jwt2 from 'hapi-auth-jwt2';
import _ from 'ramda';
import Path from 'path';
const Inert = require('@hapi/inert');

import * as ZipkinInstrumentation from 'zipkin-instrumentation-hapi';
import { tracer } from './zipkin';

import AuthenticationRouter from './authentication/router';
import DiagnosticRouter from './diagnostic/router';
import UserRouter from './user/router';
import FileRouter from './files/router';

import authenticationHandler from './authentication/handler';
import userHandler from './user/handler';
import fileHandler from './files/handler';
import { FileStorageBackend } from './files/types';
import { EmailGateWay } from './email/types';
import EmailService from './email/emailService';
import todoHandler from './todo/handler';
import TodoRouter from './todo/router'

export const init = async (
    config,
    emailGateWay: EmailGateWay,
    storageBackend: FileStorageBackend,
) => {
    // Hapi JS server initialization
    const server = Hapi.server({
        port: config.PORT,
        host: 'localhost',
        routes: {
            files: {
                relativeTo: Path.join(__dirname, '..', 'user-media'),
            },
            cors: {
                origin: ['*'],
                // an array of origins or 'ignore'
            },
        },
    });

    // Swagger configuration
    const swaggerOptions = {
        info: {
            title: 'Hapi API Documentation',
            version: '0.0.1',
        },
        host: `${config.BASE_URL}`,
        securityDefinitions: {
            jwt: {
                type: 'apiKey',
                name: 'Authorization',
                in: 'header',
            },
        },
        security: [{ jwt: [] }],
    };

    // Hapi js plugins
    await server.register([
        Inert,
        Vision,
        {
            plugin: HapiSwagger,
            options: swaggerOptions,
        },
        {
            plugin: Jwt2,
        },
    ]);

    const env = {
        config: config,
    };

    // Handlers
    const emailServiceObj = EmailService(emailGateWay);

    const userHandlerObj = userHandler(env, emailServiceObj);

    const authenticationHandlerObj = authenticationHandler(env);
    const fileHandlerObj = fileHandler(env, storageBackend);
    const todoHandlerObj = todoHandler(env);

    // Authentications
    server.auth.strategy('jwt', 'jwt', {
        key: config.JWT_SECRET,
        validate: authenticationHandlerObj.validateJWTToken,
    });

    server.auth.default('jwt');

    // Setup router

    AuthenticationRouter(server, authenticationHandlerObj);
    DiagnosticRouter(server);
    UserRouter(server, userHandlerObj);
    FileRouter(server, fileHandlerObj);
    TodoRouter(server,todoHandlerObj);

    server.route({
        method:'GET',
        path:'/',
        handler: async (request:any,h:any)=>{
            return h.response(`Welcome to Hapi Seed`).code(200);
        },
        options:{
            auth:false,
            tags: ['api'],
            description: 'start',
            notes: 'start page',
        }
    })


    await server.initialize();
    return {
        server,
        handlers: {
            authHandler: authenticationHandlerObj,
            userHandler: userHandlerObj,
        },
    };
};
