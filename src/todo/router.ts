import * as Hapi from '@hapi/hapi';
// import { request } from 'chai';
import { isLeft } from 'fp-ts/lib/Either';
import Joi from 'joi';
import * as _ from 'ramda';
import {todoSchema} from './types'


export default function(server: Hapi.Server,todoHandler:any){
    server.route([
        {
        method:'POST',
        path:'/todo',
        handler: async (request: any,h: any) => {
                const data = request.payload;
                const response = await todoHandler.createTodo(data);
                if(isLeft[response]){
                    return h.response('Error').code(404);
                }
                return h.response({todoId: response.right}).code(201);
        },
        options:{
            validate:{
                payload:todoSchema
            },
            tags: ['api'],
            description: 'todo',
            notes: 'create todo',
        }
    },
    {
        method:'GET',
        path:'/todos',
        handler: async (request:any,h:any) => {
            const result = await todoHandler.getAllTodo();
                if(isLeft(result)){
                    return h.response('No data found').code(404);
                }
                return h.response(result.right).code(200);
        },
        options:{
            tags: ['api'],
            description: 'todo',
            notes: 'get all todo',
        }
    },
    {
        method:'DELETE',
        path:'/todo/{id}',
        handler: async(request:any,h:any)=> {
            const id = request.params.id;
                const result = await todoHandler.deleteTodo(id);
                if(isLeft(result)){
                    return h.response(result.left).code(404)
                }
                return h.response('Todo Deleted !!').code(200);
        },
        options:{
            validate:{
                params: Joi.object({
                    id: Joi.number().required()
                })
            },
            tags: ['api'],
            description: 'todo',
            notes: 'delete todo by id',
        }
    },
    {
        method:'PUT',
        path:'/todo/{id}',
        handler: async (request:any,h:any)=>{
            const id = request.params.id;
            const details = request.payload;
            const result = await todoHandler.updateTodo(id,details);
            if(isLeft(result)){
                return h.response('Todo ID not available').code(404);
            }
            return h.response(`Todo ID: ${id} updated !!`).code(200);
        },
        options:{
            validate:{
                params: Joi.object({
                    id: Joi.number().required()
                }),
                payload:todoSchema
            },
            tags: ['api'],
            description: 'todo',
            notes: 'update todo by id',
        }
    }

])
}