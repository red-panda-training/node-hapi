import * as Joi from 'joi';

export interface todoDetails {
    todoName:String,
    todoDescription:String
}

export const todoSchema = Joi.object({
    todoName: Joi.string().min(7).required(),
    todoDescription: Joi.string().required(),
});


export type TodoCreatedFailed = 'wrong-data';
export type TodoId = Number;
export type TodoList = Array<Object>;
export type FetchingError = 'todo-fetching-failed';
export type TodoFailed = 'todo-id-not-available';
export type TodoCount = Number;