import { Either, isLeft, right,left } from 'fp-ts/lib/Either';
import {todoCreated,todoDeleted,todoListed,todoUpdated,todoFailed} from '../logEvents';
import { todoDetails,TodoId,TodoCreatedFailed,TodoList,FetchingError,TodoFailed,TodoCount } from './types';
import * as repo from './repo';
import _ from 'ramda';


export default function todoHandler(env:Object){
    return {
        //CREATING TODO ITEM
        createTodo: async(
            todoDetails: todoDetails
            ): Promise<Either<TodoCreatedFailed,TodoId>> => {
                const result = await repo.createTodo(todoDetails);
                if(isLeft(result)){
                    todoFailed({
                        todoName: todoDetails.todoName,
                        reason:result.left
                    });
                    return result;
                }
                todoCreated({
                    todoId: result.right,
                })
                return right(result.right);
        },
        //DELETING TODO ITEM BY ID
        deleteTodo: async (id:TodoId):Promise<Either<TodoFailed,TodoCount>> => {
            const deleted = await repo.deleteTodoById(id);
            if(isLeft(deleted)){
                todoFailed({
                    todoId: id,
                    reason:'todo-id-not-available'
                });
                return left('todo-id-not-available');
            }
            todoDeleted({
                todoId: deleted.right
            })
            return right(deleted.right);
        },
        //UPDATING TODO ITEM BY ID
        updateTodo: async (id:TodoId, details:todoDetails):Promise<Either<TodoFailed,TodoCount>> => {
            const result = await repo.updatedTodoById(id,details);
            if(isLeft(result)){
                todoFailed({
                    todoId: id,
                    reason:'todo-id-not-available'
                })
                return left('todo-id-not-available');
            }
            todoUpdated({
                todoId:id
            })
            return right(result.right);
        },
        //FETCHING ALL TODO ITEMS
        getAllTodo: async (): Promise<Either<FetchingError,TodoList>> => {
            const result = await repo.getAllTodos();
            if(isLeft(result)){
                todoFailed({
                    reason:'no-data-found'
                })
                return left('todo-fetching-failed');
            }
            todoListed({
                count:result.right.length
            });
            return right(result.right);
        }
    }
}