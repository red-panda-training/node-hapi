import db from '../db';
import { left, right, isLeft } from 'fp-ts/lib/Either';
import { todoDetails,TodoId,TodoCreatedFailed } from './types';
import _ from 'ramda';

const todoTableName = "todo_items";

export async function createTodo(details:todoDetails) {
    
        const row =  await db(todoTableName).insert({
            todoName: details.todoName,
            todoDescription: details.todoDescription
        }).returning('id');

        return right(row[0]);
}


export async function getAllTodos(){
    const result =  await db.select('*').from(todoTableName);
    if(_.isEmpty(result)){
        return left(result);
    }
    return right(result);
}


export async function deleteTodoById(id:Number){
    const result = await db(todoTableName).where('id',id).del();
    if(result < 1){
        return left(result);
    }
    return right(result);
}

export async function updatedTodoById(id:Number,details:todoDetails){
    const modifiedDate = new Date();
    const result = await db(todoTableName).where({id:id}).update({
        todoName: details.todoName,
        todoDescription: details.todoDescription,
        modified_date:modifiedDate
    });
    if(result < 1){
        return left(result);
    }
    return right(result);
}
