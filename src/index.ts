/* eslint-disable fp/no-throw */
/* eslint-disable no-console */
/* eslint-disable fp/no-nil */
import * as Server from './server';
import config from './config';
import localFileStorage from './files/storageBackend.local';
import s3Storage from './files/storageBackend.s3';
import SESEmailGateway from './email/SESGateway';
import { FileStorageBackend } from './files/types';
import * as HapiSentry from 'hapi-sentry';
import { logger } from './logger';
import _ from 'ramda';
import { EmailGateWay, Email } from './email/types';

// Catch unhandled unexpected exceptions
process.on('uncaughtException', (error: Error) => {
    logger.error({ error: error, type: 'uncaughtException' });
    console.error(`uncaughtException ${error.message}`);
});

// Catch unhandled rejected promises
process.on('unhandledRejection', (reason: any) => {
    logger.error({ reason: reason, type: 'unhandledRejection' });
    console.error(`unhandledRejection ${reason}`);
});

const start = async () => {
    try {
        const dummyEmailGateway: EmailGateWay = {
            sendEmail: async (email: Email) => {
                console.log(email);
            },
        };

        const storageBackend: FileStorageBackend =
            config.STORAGE_BACKEND == 'localFileStorage'
                ? localFileStorage(config)
                : s3Storage(config);

        const emailGateWay =
            config.EMAIL_GATEWAY == 'localConsole'
                ? dummyEmailGateway
                : SESEmailGateway(config);

        const { server } = await Server.init(
            config,
            emailGateWay,
            storageBackend,
        );

        if (!_.isNil(config.SENTRY_DSN)) {
            await server.register([
                {
                    plugin: HapiSentry,
                    options: {
                        client: {
                            dsn: config.SENTRY_DSN,
                            release: config.RELEASE_VERSION,
                            environment: config.SERVER_ENVIRONMENT,
                        },
                    },
                },
            ]);
            logger.info({ name: 'init', metadata: 'Initialized the sentry' });
        }

        await server.start();
        console.log('Server running at:', server.info.uri);
    } catch (err) {
        console.error('Error starting server: ', err.message);
        throw err;
    }
};

start();
