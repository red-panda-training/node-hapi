import _ from 'ramda';
import { Either, left, right, isLeft } from 'fp-ts/lib/Either';
import { FileDetails } from './types';
import db from '../db';

const fileTableName = 'files';

export async function saveFile(fileDetails: FileDetails) {
    const row = await db(fileTableName)
        .insert({
            id: fileDetails.fileId,
            name: fileDetails.fileName,
            fileType: fileDetails.mimetype,
            isUsed: false,
            metadata: {
                originalFilename: fileDetails.originalFilename,
            },
        })
        .returning('id');

    return right(row[0]);
}
