import config from '../config';
import _ from 'ramda';

export default function getPath(fileName: any) {
    if (_.isNil(fileName) || _.isEmpty(fileName)) {
        return null;
    }
    return `${config.STATIC_FILES_BASE_PATH}/${fileName}`;
}
