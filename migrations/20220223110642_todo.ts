import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    const query = `
    CREATE TABLE todo_items
    (
        "todoName" VARCHAR(255) NOT NULL,
        "todoDescription" VARCHAR(4000),
        id serial PRIMARY KEY,
        "creation_date" timestamp with time zone NOT NULL DEFAULT now(),
        "modified_date" timestamp with time zone
    );
    `
    return knex.schema.raw(query);  
}


export async function down(knex: Knex): Promise<void> {
    const query = `
    DROP TABLE IF EXISTS todo_items cascade;
    `;
    return knex.schema.raw(query);
}

