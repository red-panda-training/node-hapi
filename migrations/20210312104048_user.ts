import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {

    const query = `

    CREATE TABLE users (
        "createdAt" timestamptz DEFAULT now(),
        "updatedAt" timestamptz DEFAULT now(),
        id serial PRIMARY KEY,
        "firstName" TEXT,
        "lastName" TEXT,
        "email" TEXT UNIQUE,
        "phoneNumber" TEXT,
        password TEXT,
        "emailVerified" Boolean default false,
        "isActive" Boolean default true, 
        metadata JSONB
    );
    
    CREATE OR REPLACE FUNCTION update_updated_at()
    RETURNS trigger AS
    $$      
    begin
     NEW."updatedAt" = now();
     RETURN NEW;
    END;
    $$
    LANGUAGE 'plpgsql';
        
    CREATE TRIGGER users_updated
    BEFORE INSERT OR UPDATE
    ON users
    FOR EACH ROW
    EXECUTE PROCEDURE update_updated_at();
    
    CREATE TABLE session (
        "sessionId" VARCHAR (50) PRIMARY KEY,
        "createdAt" timestamptz DEFAULT now(),
        "lastUsed" timestamptz DEFAULT now(),
        "userId" INT REFERENCES users(id)
    );

    CREATE TABLE todo_items
    (
        "todoName" VARCHAR(255) NOT NULL,
        "todoDescription" VARCHAR(4000),
        id serial PRIMARY KEY,
        "creation_date" timestamp with time zone NOT NULL DEFAULT now(),
        "modified_date" timestamp with time zone
    )
    `;
    return knex.schema.raw(query);    
}


export async function down(knex: Knex): Promise<void> {

    const query = `
    DROP TABLE IF EXISTS session;
    DROP TABLE IF EXISTS users cascade;
    DROP TABLE IF EXISTS todo_items cascade;
    `;
    return knex.schema.raw(query);
}

